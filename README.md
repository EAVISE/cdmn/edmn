# eDMN

DMN with support for an epistemic operator.

More explanations will be added soon.

## Installation and usage

```
git clone https://gitlab.com/EAVISE/cdmn/edmn eDMN
cd eDMN
pip3 install -r requirements.txt
```

To use eDMN, simply supply it a .xlsx file containing an eDMN model.

```
python solver.py Examples/loan.xlsx
```

# Contributors

Simon Vandevelde, Djordje Markovic, Marc Denecker, Joost Vennekens
KU Leuven - KRR group

This project was forked from the [cDMN solver](https://gitlab.com/EAVISE/cdmn/cdmn-solver): also see their contributor list.

