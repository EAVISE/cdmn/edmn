import argparse
from edmn.glossary import Glossary
from edmn.interpret import VariableInterpreter
from edmn.idply import Parser
import sys
from edmn.table_operations import (fill_in_merged, identify_tables,
                                   find_glossary, find_execute_method,
                                   replace_with_error_check,
                                   create_voc, create_main,
                                   create_struct, create_theory,
                                   create_display)
# from post_process import merge_definitions

def cli():
    # Parse the arguments.
    argparser = argparse.ArgumentParser(description='Turn eDMN tables into FO(.) OEL.')
    argparser.add_argument('--version', '-v', action='version',
                           version='1.0.0')
    argparser.add_argument('path_to_file', metavar='path_to_file', type=str,
                           help='the path to the xlsx or xml file')
    argparser.add_argument('-o', '--outputfile', metavar='outputfile',
                           type=str,
                           default=None,
                           help='the name of the outputfile')

    # Open the file on the correct sheet and read all the tablenames.
    args = argparser.parse_args()
    spec = to_OEL(args.path_to_file, args.outputfile)
    if spec:
        print(spec)


def to_OEL(path_to_file, outputfile=None):
    """
    The main function for the eDMN solver.
    """
    filepath = path_to_file
    if filepath.endswith('.xlsx'):
        xml = False
        sheetnames = []
        sheets = fill_in_merged(filepath, sheetnames)
        tables = identify_tables(sheets)

    elif filepath.endswith('.dmn') or filepath.endswith('.xml'):
        xml = True
        from edmn.parse_xml import XMLparser
        with open(filepath, 'r') as f:
            p = XMLparser(f.read())
            tables = p.get_tables()

    else:
        raise IOError("Invalid filepath")

    g = Glossary(find_glossary(tables))

    i = VariableInterpreter(g)
    parser = Parser(i)

    # Create the main blocks.
    # struct = create_struct(tables, parser, g)
    voc = create_voc(g)
    theory = create_theory(tables, parser)
    file_path = None
    print('Done parsing.')
    if len(parser.parsing_errors) != 0:
        print("Errors detected in specification.\nUnable to parse headers:")
        for header, error_list in parser.parsing_errors.items():
            print(f"\tin {header}:")
            for error in error_list:
                print(f"\t\t{error}")
        print("No output was created.")
        return

    # If an output file is listed, write to it.
    if outputfile:
        file_path = outputfile
        fp = open(file_path, 'w')
        fp.write(voc)
        fp.write(theory)
        fp.close()

    # If no options were supplied, print the specification.
    if outputfile is None:
        return voc + theory

if __name__ == "__main__":
    main()
